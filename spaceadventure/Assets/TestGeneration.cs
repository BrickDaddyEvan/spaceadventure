﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestGeneration : MonoBehaviour
{
    public GameObject[] tiles;
    public Transform startingTile;
    public GameObject[] emptyArray;
    // Start is called before the first frame update
    void Start()
    {
        GenerateEarth();
    }

    // Update is called once per frame
    void Update()
    {


    }
    void GenerateEarth()
    {
            for (int i = 0; i < tiles.Length; i++)
            {
                Instantiate(tiles[Random.Range(0, tiles.Length)], new Vector2(1, i), tiles[i].transform.rotation);
                for (int j = 0; j < tiles.Length; j++)
                {
                    Instantiate(tiles[Random.Range(0, tiles.Length)], new Vector2(j, i), tiles[i].transform.rotation);
                    
                }
            }    
        }
    }

