﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed = 5; //used to set the speed of the player

    void Start()
    {
        
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))//when shift is held increase player speed to 5
        {
            playerSpeed = 5;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))//when finger is off of shift decrease speed back to 3
        {
            playerSpeed = 3;
        }
        if (Input.GetKeyDown(KeyCode.C))//when c is held decrease player speed to 1.5
        {
            playerSpeed = 1.5f;
        }
        if (Input.GetKeyUp(KeyCode.C))//when finger is off of c increase speed back to 3
        {
            playerSpeed = 3;
        }
        if (Input.GetKey(KeyCode.W))// makes player go up when w is pressed
        {
            transform.Translate(Vector3.up * Time.deltaTime * playerSpeed); 
        }
        if (Input.GetKey(KeyCode.A))// makes player go left when a is pressed
        {
            transform.Translate(Vector3.left * Time.deltaTime * playerSpeed);        
        }
        if (Input.GetKey(KeyCode.S))// makes player go down when s is pressed
        {
            transform.Translate(Vector3.down * Time.deltaTime * playerSpeed);
        }
        if (Input.GetKey(KeyCode.D))// makes player right up when d is pressed
        {
            transform.Translate(Vector3.right * Time.deltaTime * playerSpeed);
        }
    }
}
